import Base: schedule

export AbstractTaskletSchema, AbstractTasklet
export TaskletSchema, TaskletGroupSchema
export Tasklet, TaskletGroup
export create, schedule
export getparam, setparam!
export getinchan, getoutchan, getinevent, getoutevent
export inchan_names, inchan_names, outchan_names, outchan_names, inevent_names, inevent_names, outevent_names, outevent_names

### Types ###

abstract type AbstractTaskletSchema end
abstract type AbstractTasklet end

mutable struct TaskletSchema <: AbstractTaskletSchema
  inputs::Vector{String}
  outputs::Vector{String}
  hooks::Dict{String,Function}
  
  in_triggers::Vector{String}
  out_triggers::Vector{String}
  triggers::Dict{String,Function}
  
  start::Union{Function,Void}
  params::Dict{String,Any}
  values::Dict{String,Any}
end
TaskletSchema(;
  inputs=String[],
  outputs=String[],
  hooks=Dict{String,Function}(),

  in_triggers=String[],
  out_triggers=String[],
  triggers=Dict{String,Function}(),

  start=nothing,
  params=Dict{String,Any}(),
  values=Dict{String,Any}(),
) = TaskletSchema(inputs, outputs, hooks, in_triggers, out_triggers, triggers, start, params, values)

mutable struct TaskletGroupSchema <: AbstractTaskletSchema
  tasklets::Dict{String,AbstractTaskletSchema}
  chan_map::Dict{Tuple{String,String},Vector{Tuple{String,String}}}
  event_map::Dict{Tuple{String,String},Vector{Tuple{String,String}}}
  inchan_expose_map::Dict{String,Tuple{String,String}}
  outchan_expose_map::Dict{String,Tuple{String,String}}
  inevent_expose_map::Dict{String,Tuple{String,String}}
  outevent_expose_map::Dict{String,Tuple{String,String}}
  param_expose_map::Dict{String,Tuple{String,String}}
  # TODO: Exposable values?
end
TaskletGroupSchema(;
  tasklets=Dict{String,AbstractTaskletSchema}(),
  chan_map=Dict{Tuple{String,String},Vector{Tuple{String,String}}}(),
  event_map=Dict{Tuple{String,String},Vector{Tuple{String,String}}}(),
  inchan_expose_map=Dict{String,Tuple{String,String}}(),
  outchan_expose_map=Dict{String,Tuple{String,String}}(),
  inevent_expose_map=Dict{String,Tuple{String,String}}(),
  outevent_expose_map=Dict{String,Tuple{String,String}}(),
  param_expose_map=Dict{String,Tuple{String,String}}()
) = TaskletGroupSchema(tasklets, chan_map, event_map, inchan_expose_map, outchan_expose_map, inevent_expose_map, outevent_expose_map, param_expose_map)

mutable struct TaskletForkSchema <: AbstractTaskletSchema
  # TODO
  tasklet::AbstractTaskletSchema
  initialSize::Int
end

mutable struct Tasklet <: AbstractTasklet
  hooks::Dict{String,Function}
  rx_chans::Dict{String,Channel}
  tx_chans::Dict{String,Vector{Pair{String,Tasklet}}}
  rx_chan_tasks::Dict{String,Task}

  triggers::Dict{String,Function}
  rx_events::Dict{String,Condition}
  tx_events::Dict{String,Vector{Pair{String,Tasklet}}}
  rx_event_tasks::Dict{String,Task}

  start_task::Union{Task,Void}
  params::Dict{String,Any}
  values::Dict{String,Any}
end

mutable struct TaskletGroup <: AbstractTasklet
  tasklets::Dict{String,AbstractTasklet}  
  inchan_expose_map::Dict{String,Tuple{String,String}}
  outchan_expose_map::Dict{String,Tuple{String,String}}
  inevent_expose_map::Dict{String,Tuple{String,String}}
  outevent_expose_map::Dict{String,Tuple{String,String}}
  param_expose_map::Dict{String,Tuple{String,String}}
end

mutable struct TaskletFork <: AbstractTasklet
  id::String
  size::Int
  tasklets::Vector{AbstractTasklet}
  # TODO: barrier_data
end

### Methods ###

function create(ts::TaskletSchema)
  # Create Tasklet
  tsk = Tasklet(
    ts.hooks,
    Dict{String,Channel}([input=>Channel(Inf) for input in ts.inputs]), # TODO: Channel typing and sizing
    Dict{String,Vector{Pair{String,Tasklet}}}([output=>Pair{String,Tasklet}[] for output in ts.outputs]),
    Dict{String,Task}(),

    ts.triggers,
    Dict{String,Condition}([input=>Condition() for input in ts.in_triggers]),
    Dict{String,Vector{Pair{String,Tasklet}}}([output=>Pair{String,Tasklet}[] for output in ts.out_triggers]),
    Dict{String,Task}(),

    ts.start == nothing ? nothing : Task(() -> ts.start(tsk)),
    deepcopy(ts.params),
    deepcopy(ts.values) # TODO: NamedTuple?
  )

  # Create channel listener tasks
  for chan in keys(tsk.rx_chans)
    tsk.rx_chan_tasks[chan] = @task begin
      while true
        value = take!(tsk.rx_chans[chan])
        tsk.hooks[chan](tsk, value)
      end
    end
  end
  
  # Create event listener tasks
  for event in keys(tsk.rx_events)
    tsk.rx_event_tasks[event] = @task begin
      while true
        wait(tsk.rx_events[event])
        tsk.triggers[event](tsk)
      end
    end
  end

  # Return tasklet
  return tsk
end
function create(tgs::TaskletGroupSchema)
  # Create TaskletGroup
  tg = TaskletGroup(
    Dict{String,AbstractTasklet}(),
    tgs.inchan_expose_map,
    tgs.outchan_expose_map,
    tgs.inevent_expose_map,
    tgs.outevent_expose_map,
    tgs.param_expose_map
  )

  # Create Tasklets
  for (name,schema) in tgs.tasklets
    tg.tasklets[name] = create(schema)
  end

  # Connect channels
  for (t1,t2vec) in tgs.chan_map
    for t2 in t2vec
      connect_chans!((tg.tasklets[t1[1]], t1[2]), (tg.tasklets[t2[1]], t2[2]))
    end
  end

  # Connect events
  for (t1,t2vec) in tgs.event_map
    for t2 in t2vec
      connect_events!((tg.tasklets[t1[1]], t1[2]), (tg.tasklets[t2[1]], t2[2]))
    end
  end

  # Return TaskletGroup
  return tg
end
function schedule(tsk::Tasklet)
  tsk.start_task != nothing && schedule(tsk.start_task)
  [schedule(task) for task in values(tsk.rx_chan_tasks)]
  [schedule(task) for task in values(tsk.rx_event_tasks)]
end
schedule(tg::TaskletGroup) = schedule.(collect(values(tg.tasklets)))
# TODO: schedule(TaskletFork)

function connect_chans!(t1::Tuple{Tasklet,String}, t2::Tuple{Tasklet,String})
  push!(t1[1].tx_chans[t1[2]], t2[2] => t2[1])
end
function connect_events!(t1::Tuple{Tasklet,String}, t2::Tuple{Tasklet,String})
  push!(t1[1].tx_events[t1[2]], t2[2] => t2[1])
end

# TODO: getters/setters for schema fields

getparam(tsk::Tasklet, name::String) = getindex(tsk.params, name)
function getparam(tsk::TaskletGroup, name::String)
  target, param = tsk.param_expose_map[name]
  if tsk.tasklets[target] isa Tasklet
    getindex(tsk.tasklets[target].params, param)
  else
    getparam(tsk.tasklets[target].params, param)
  end
end
setparam!(tsk::Tasklet, name::String, value) = setindex!(tsk.params, value, name)
function setparam!(tsk::TaskletGroup, name::String, value)
  target, param = tsk.param_expose_map[name]
  if tsk.tasklets[target] isa Tasklet
    setindex!(tsk.tasklets[target].params, value, param)
  else
    setparam!(tsk.tasklets[target].params, param, value)
  end
end

getinchan(tsk::Tasklet, name::String) = getindex(tsk.rx_chans, name)
getoutchan(tsk::Tasklet, name::String) = getindex(tsk.tx_chans, name)
getinevent(tsk::Tasklet, name::String) = getindex(tsk.rx_events, name)
getoutevent(tsk::Tasklet, name::String) = getindex(tsk.tx_events, name)
# FIXME: TaskletGroup

inchan_names(ts::TaskletSchema) = ts.inputs
inchan_names(ts::TaskletGroupSchema) = collect(keys(ts.inchan_expose_map))
outchan_names(ts::TaskletSchema) = ts.outputs
outchan_names(ts::TaskletGroupSchema) = collect(keys(ts.outchan_expose_map))
inevent_names(ts::TaskletSchema) = ts.in_triggers
inevent_names(ts::TaskletGroupSchema) = collect(keys(ts.inevent_expose_map))
outevent_names(ts::TaskletSchema) = ts.out_triggers
outevent_names(ts::TaskletGroupSchema) = collect(keys(ts.outevent_expose_map))
