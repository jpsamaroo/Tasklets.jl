__precompile__(true)

# FIXME: Wrapping with module causes possible world age issues?
module Tasklets

using MacroTools

include("tasklet.jl")
include("macros.jl")

# Include library
@tregisterroot @tregistertree "../lib/"

end
