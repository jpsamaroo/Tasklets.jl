export @tschema, @tgroupschema
export @inevent, @outevent, @inchan, @outchan
export @inevent_str, @outevent_str, @inchan_str, @outchan_str
export @tstart, @tevent, @trecv, @tsend, @tnotify, @tnotify_str
export @tparam, @tparam_str, @tvalue, @tvalue_str, @tget, @tget_str, @tset, @tset_str
export @tautorecv_str, @tautoexpose_str
export @teventmap, @tchanmap

macro tregisterroot(ex::Expr)
  esc(:(__REGISTRY__ = Dict(); $ex; __REGISTRY__))
end
macro tregistertree(path::String)
  # FIXME
  #=esc(quote
    __old_registry = __REGISTRY__
    for (root, dirs, files) in walkdir($path)
      __REGISTRY__ = dirname(rstrip(root, Base.Filesystem.path_separator))
      for file in files
        println(joinpath(root, file)) # path to files
        
      end
    end
    __REGISTRY__ = __old_registry
  end)=#
end
macro tregistertree(prefix::String, path::String)
  # FIXME
end
macro tregisterfile(path::String)
  esc(:(__REGISTRY__ = include($path)))
end
macro tregisterfile(prefix::String, path::String)
  esc(:(__REGISTRY__[$prefix] = include($path)))
end
macro tregister(name::String, ex::Expr)
  esc(:(__REGISTRY__[$name] = $ex))
end

macro tschema(ex::Expr)
  new_ex = Expr(:block)
  push!(new_ex.args, :(__SCHEMA__ = TaskletSchema()))

  if @capture(ex, statements__)
    for statement in statements
      # FIXME: Proper type conversion
      if @capture(statement, name_ == initval_ <: typ_)
        push!(new_ex.args, :(__SCHEMA__.params[$name] = $initval))
      elseif @capture(statement, name_ == initval_)
        push!(new_ex.args, :(__SCHEMA__.params[$name] = $initval))
      elseif @capture(statement, name_ := initval_ <: typ_)
        push!(new_ex.args, :(__SCHEMA__.values[$name] = $initval))
      elseif @capture(statement, name_ := initval_)
        push!(new_ex.args, :(__SCHEMA__.values[$name] = $initval))
      else
        push!(new_ex.args, statement)
      end
    end
  else
    error("Invalid tasklet schema:\n$ex")
  end

  push!(new_ex.args, :(__SCHEMA__))
  return esc(new_ex)
end
macro tgroupschema(ex::Expr)
  new_ex = Expr(:block)
  push!(new_ex.args, :(__SCHEMA__ = TaskletGroupSchema()))

  if @capture(ex, statements__)
    for statement in statements
      if @capture(statement, name_ == tschema_)
        push!(new_ex.args, :(__SCHEMA__.tasklets[$name] = $tschema))
      else
        push!(new_ex.args, statement)
      end
    end
  else
    error("Invalid tasklet schema:\n$ex")
  end

  push!(new_ex.args, :(__SCHEMA__))
  return esc(new_ex)
end
# TODO: @tschemafork

# TODO: Regular macro alternatives
macro inchan_str(name::String)
  esc(:(push!(__SCHEMA__.inputs, $name)))
end
macro outchan_str(name::String)
  esc(:(push!(__SCHEMA__.outputs, $name)))
end
macro inevent_str(name::String)
  esc(:(push!(__SCHEMA__.in_triggers, $name)))
end
macro outevent_str(name::String)
  esc(:(push!(__SCHEMA__.out_triggers, $name)))
end
macro tchanmap(ex::Expr)
  @assert @capture(ex, T1_ => T2_)
  t1, t2 = (String.(split(T1, "."))...,), (String.(split(T2, "."))...,)
  esc(quote
    if !haskey(__SCHEMA__.chan_map, $t1)
      __SCHEMA__.chan_map[$t1] = Tuple{String,String}[]
    end
    push!(__SCHEMA__.chan_map[$t1], $t2)
  end)
end
macro teventmap(ex::Expr)
  @assert @capture(ex, T1_ => T2_)
  t1, t2 = (String.(split(T1, "."))...,), (String.(split(T2, "."))...,)
  esc(quote
    if !haskey(__SCHEMA__.event_map, $t1)
      __SCHEMA__.event_map[$t1] = Tuple{String,String}[]
    end
    push!(__SCHEMA__.event_map[$t1], $t2)
  end)
end

macro tget_str(name::String)
  esc(:(__TASKLET__.values[$name]))
end
macro tget(name::String)
  esc(:(__TASKLET__.values[$name]))
end
macro tset(name::String, value)
  esc(:(__TASKLET__.values[$name] = $value))
end
macro tparam_str(name::String)
  esc(:(__TASKLET__.params[$name]))
end
macro tparam(name::String)
  esc(:(__TASKLET__.params[$name]))
end
macro tvalue()
  esc(:(__VALUE__))
end

macro tsend(name::String, value)
  esc(quote
    for __OTHER__ in __TASKLET__.tx_chans[$name]
      put!(__OTHER__[2].rx_chans[__OTHER__[1]], $value)
    end
  end)
end
macro tnotify_str(name::String)
  esc(quote
    for __OTHER__ in __TASKLET__.tx_events[$name]
      notify(__OTHER__[2].rx_events[__OTHER__[1]])
    end
  end)
end
macro tnotify(name::String)
  esc(quote
    for __OTHER__ in __TASKLET__.tx_events[$name]
      notify(__OTHER__[2].rx_events[__OTHER__[1]])
    end
  end)
end

macro tstart(ex::Expr)
  esc(:(__SCHEMA__.start = (__TASKLET__) -> $ex))
end
macro trecv(name::String, ex::Expr)
  esc(:(__SCHEMA__.hooks[$name] = (__TASKLET__, __VALUE__) -> $ex))
end
macro tevent(name::String, ex::Expr)
  esc(:(__SCHEMA__.triggers[$name] = (__TASKLET__) -> $ex))
end
macro tautorecv_str(name::String)
  esc(:(__SCHEMA__.hooks[$name] = (__TASKLET__, __VALUE__) -> @tset($name, __VALUE__)))
end

macro tautoexpose_str(name::String)
  esc(quote
    target = __SCHEMA__.tasklets[$name]
    for chan in inchan_names(target)
      __SCHEMA__.inchan_expose_map["$($name).$chan"] = ($name,chan)
    end
    for chan in outchan_names(target)
      __SCHEMA__.outchan_expose_map["$($name).$chan"] = ($name,chan)
    end
    for event in inevent_names(target)
      __SCHEMA__.inevent_expose_map["$($name).$event"] = ($name,event)
    end
    for event in outevent_names(target)
      __SCHEMA__.outevent_expose_map["$($name).$event"] = ($name,event)
    end
    for param in keys(target.params)
      __SCHEMA__.param_expose_map["$($name).$param"] = ($name,param)
    end
  end)
end

# TODO: tdoc?

# TODO: tforkid
# TODO: tforksize
# TODO: tforkidall
# TODO: tforksizeall
# TODO: tinfork
# TODO: tbarrier
# TODO: tballot
