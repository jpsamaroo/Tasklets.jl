# Outputs a pre-defined value on event
@tregister "value" @tschema begin
  "value" == "Default Value!"

  inevent"send"
  outchan"output"

  @tevent "send" begin
    @tsend "output" tparam"value"
  end
end

# Gates a pair of channels
@tregister "gate" @tschema begin
  value == nothing

  inchan"input"
  outchan"output"

  @trecv "input" @tset "value" @tvalue
  @tevent "gate" begin
    @tsend "output" tget"value"
  end
end

# Prints a line to stdout
@tregister "println" @tschema begin
  inchan"input"

  @trecv "input" begin
    println(@tvalue)
  end
end

@tregister "freqtimer" @tschema begin
  # Parameters
  "freq" == 100 <: Float64

  # Input Events
  inevent"start"
  inevent"stop"

  # Output Events
  outevent"tick"

  # Event Handlers
  @tevent "start" begin
    @tset "running" true
  end
  @tevent "stop" begin
    @tset "running" false
  end

  # Start Handler
  @tstart begin
    # Start timer
    @tset "running" true
    while true
      sleep(1/tparam"freq")
      if tget"running"
        tnotify"tick"
      end
    end
  end
end

# Runs a timer after a specified delay
#=@tregister "oneshottimer" @tschema begin
  # Parameters
  "delay" == 10

  # Input Events
  inevent"start"
  inevent"reset"

  # Output Events
  outevent"tick"

  # Event Handlers
  @tevent "start" begin
    @tset "running" true

  end
  @tevent "reset" begin
    if tget"running"
      
      @tset "running" false
    end
  end

  # Start Handler
  @tstart begin
    # Start timer
    @tset "running" true
    while true
      sleep(1/tparam"freq")
      if tget"running"
        tnotify"tick"
      end
    end
  end
end

TaskletSchema("fpslock";
  params=Dict(
    "fps"=>1
  ),
  start=tsk->ttemp(tsk,"last",time_ns()),
  in_triggers=["stop"],
  out_triggers=["start"],
  triggers=Dict(
    "stop"=>tsk->begin
      diff = (time_ns-ttemp(tsk,"last")) / (10^9)
      sleep(abs((1/tparam(tsk,"fps"))-diff))
      trigger(tsk,"start")
    end
  )
)=#
