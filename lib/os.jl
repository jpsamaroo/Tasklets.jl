@tregister "procspawn" @tschema begin
  "procname" == ""
  "procname" := ""

  inchan"procname"
  inevent"spawn"

  tautorecv"procname"
  @tevent "spawn" begin
    # TODO: Capture and relay STDIO
    # TODO: Watch for exit
    spawn(tget"procname")
  end
  @tstart begin
    @tset "procname" tparam"procname"
  end
end
